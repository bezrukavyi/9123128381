1. Intro
2. Development
 2.1 Dockerfile
 2.2 Docker compose
3. Staging
 3.1 Nginx Dockerfile
 3.2 Env credentials
 3.3 AWS base configuration
 3.4 ECS staging configuration
 3.5 Launch staging application
4. Production
 4.1 ECS production configuration (RDS, ElasticCache, LoadBalancer)
 4.2 Launch production application
5. CI
 5.1 Deploy script
 5.2 CircleCI configuration
 5.3 Test commit
