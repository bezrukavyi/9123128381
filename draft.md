## EC2

Теперь нужно, чтобы файл ecs.config оказался на инстансе на EC2 для его конфигурации. Для этого создадим bash-команду, которая запустится на момент инициализации инстанса и скачает файл `ecs.config` S3.

```bash
touch aws/import-ecs-config
```

```sh
#!/bin/bash

yum install -y aws-cli
aws s3 cp s3://spreeproject-production/ecs.config /etc/ecs/ecs.config
```

Для большей безопасности, вход на инстанс будет осуществляться по ключу, который мы создадим:

```bash
aws ec2 create-key-pair \
  --key-name spreeproject-aws-admin \
  --query 'KeyMaterial' \
  --output text > ~/.ssh/spreeproject-aws-admin.pem

chmod 400 ~/.ssh/spreeproject-aws-admin.pem
```

Если в дальнейшем нужно будет осуществить вход по ssh-ключу, просто обновите security group:

```bash
aws ec2 authorize-security-group-ingress \
  --group-id $SERVER_APP_SG \
  --protocol tcp \
  --port 22 \
  --cidr 0.0.0.0/0

# Команда для подключения к определенному инстансу по ssh-ключу:
ssh -i ~/.ssh/spreeproject-aws-admin.pem ec2-user@$EC2_PUBLIC_DOMAIN
```

AWS предоставляет ряд готовых [images](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/launch_container_instance.html) для инстансов. Мы воспользуемся образом `ami-0a6be20ed8ce1f055`.

```bash
aws ec2 run-instances \
  --image-id ami-0a6be20ed8ce1f055 \
  --count 3 \
  --instance-type t2.micro \
  --iam-instance-profile Name=ecsInstanceRole \
  --user-data file://aws/import-ecs-config \
  --key-name spreeproject-aws-admin \
  --security-group-ids $SERVER_APP_SG
```

Возможно, при попытке создания 3 инстансов, вы получите сообщение о превышении лимита количества создаваемых инстансов. В таком случае, обратитесь в поддержку с просьбой [увеличить этот лимит](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-resource-limits.html#request-increase).

После создания инстансов вы можете проверить его состояние:

```bash
aws ec2 describe-instance-status --instance-id $INSTANCE_ID
```

После успешного завершения инициализации вы можете увидеть, что инстансы были добавлены в кластер `spreeprojectProduction`

```bash
aws ecs list-container-instances --cluster spreeprojectProduction
```







Dev staging




```bash
  # aws ec2 run-instances \
  #   --image-id ami-0a6be20ed8ce1f055 \
  #   --count 1 \
  #   --instance-type t2.micro \
  #   --iam-instance-profile Name=AmazonEC2ContainerServiceforEC2Role \
  #   --user-data file://aws/ecs-config \
  #   --key-name asdasdasdasd \
  #   --security-group-ids $SERVER_APP_SG
```

```bash
    CLUSTER_NAME=spreeproject-production

    # Create ECS-CLi configuration for future cluster
    ecs-cli configure --region us-east-1 --cluster $CLUSTER_NAME --config-name $CLUSTER_NAME

    # create ecs cluster with EC2 instances
    ecs-cli up \
      --keypair asdasdasdasd \
      --capability-iam \
      --size 1 \
      --instance-type t2.micro \
      --vpc vpc-0e934a76 \
      --subnets subnet-20ae1647 \
      --image-id ami-0a6be20ed8ce1f055 \
      --security-group $SERVER_APP_SG \
      --cluster-config $CLUSTER_NAME \
      --verbose

    # create task definition for a docker container
    ecs-cli compose --file docker-compose.dev.yml --project-name $CLUSTER_NAME --verbose create

    # create elb & add a dns CNAME for the elb dns
    aws elb create-load-balancer \
      --load-balancer-name $CLUSTER_NAME \
      --listeners "Protocol=HTTP,LoadBalancerPort=80,InstanceProtocol=HTTP,InstancePort=8080" \
      --subnets subnet-e49c19b8 subnet-20ae1647 subnet-319d1a1f \
      --security-groups $BALANCER_SG

    aws elb modify-load-balancer-attributes \
      --load-balancer-name $CLUSTER_NAME \
      --load-balancer-attributes "{\"ConnectionSettings\":{\"IdleTimeout\":5}}"

    # create service with above created task definition & elb
    aws ecs create-service \
      --service-name "service-$CLUSTER_NAME" \
      --cluster "$CLUSTER_NAME" \
      --task-definition "spreeproject-production:3" \
      --load-balancers "loadBalancerName=$CLUSTER_NAME,containerName=web_server,containerPort=8080" \
      --desired-count 1 \
      --deployment-configuration "maximumPercent=200,minimumHealthyPercent=50"
```


```bash
ecs-cli scale \
  --capability-iam \
  --size 1 \
  --cluster $CLUSTER_NAME \
  --verbose
```
