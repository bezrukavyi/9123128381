## Запускаем Development окружение

### Постановка задачи

В этом разделе мы запустим наше Spree-приложение и все зависимые сервисы (PostgreSQL, Redis и т.д.) на локальной машине.

Стек технологий, который мы будем использовать, включает:

- [Docker](https://www.docker.com/)
- [Docker Compose](https://docs.docker.com/compose/)
- [Ruby](https://www.ruby-lang.org/en/)
- [Ruby on Rails](https://rubyonrails.org/)
- [Redis](https://redis.io/)
- [PostgreSQL](https://www.postgresql.org/)
- [Sidekiq](https://sidekiq.org/)

Схема инфраструктуры, которую мы хотим развернуть:

_Подпись: Инфраструктура приложения для_ _development_ _окружения_


**Server App:**

- Описание: Основное приложение на RubynRails.
- Образ: Будет написан собственный образ на основе образа Ruby из [Docker](https://hub.docker.com/_/ruby)[Hub](https://hub.docker.com/_/ruby).

- Коммуникация: с Database, с MemoryStore для создания задач, которые будут выполнять WorkerApp

**Database:**

- Описание: База данных основного приложения.
- Образ: Образ Postgres с официального репозитория [Docker](https://hub.docker.com/_/postgres)[Hub](https://hub.docker.com/_/postgres).

- Коммуникация с Server App и Worker App.

**Memory Store:**

- Описание: In-memorystore для хранения информации для background задач.
- Образ: Образ Redis с официального репозитория [Docker](https://hub.docker.com/_/redis)[Hub](https://hub.docker.com/_/redis).

- Коммуникация: c Server App и Worker App.

**Worker App:**

- Описание: SidekiqServer нашего приложения, которое выполняет background задачи.
- Образ: Тот же образ, что и у ServerApp.

- Коммуникация: с Database и MemoryStore в качестве SidekiqServer.

### Решение задачи

Для этого нам понадобится:

- Установить Docker на локальной машине
- Создать Docker образ Rails приложения
- Создать compose-файл для запуска Rails-приложения и зависимых сервисов (Redis, PostgreSQL, Sidekiq)

### Устанавливаем Docker

[Install Docker Community Edition](https://docs.docker.com/install/) for Linux, Mac or Windows

### Упаковываем Rails-приложения в Docker образ

#### Что такое Dockerfile и как он работает

Мы с вами уже упомянули понятие образа, который является шаблоном для каждого запущенного контейнера. [Dockerfile](https://docs.docker.com/engine/reference/builder) представляет из себя инструкцией для сборки образ вашего ПО.




_Подпись: Пошаговый принцип работы_ _Dockerfile __,_ _Image__ ,_ _Container_

Теперь рассмотрим Dockerfile нашего приложения и из каких слоев оно будет состоять.

Каждая команда (RUN, ENTRYPOINT, CMD и другие) в Dockerfile вызывает создание нового слоя при сборке образа. Структура связей между слоями в Docker - иерархическая. Имеется некий базовый слой, на который &quot;накладываются&quot; остальные слои.


#### Безопасность образа

По умолчанию, все команды по сборке образа и процессы внутри контейнера выполняются от имени root-пользователя. Такой подход не безопасен. Поэтому для запуска приложения мы используем www-data пользователя. Делаем мы это с помощью команды USER, которая задает пользователя, от имени которого будут выполняться все перечисленные ниже команды, включая RUN, ENTRYPOINT и CMD.

#### Полезные ссылки

Полный список инструкций для Dockerfile найдете [тут](https://docs.docker.com/develop/develop-images/dockerfile_best-practices/#dockerfile-instructions). Пожалуйста, ознакомьтесь с ними перед тем, как двигаться дальше по туториалу.

#### Описываем образ с помощью Dockerfile

Теперь рассмотрим структуру нашего образа уже со стороны реализации.

В качество демо-приложения в туториале мы будем использовать Spree-приложение.

1. Копируем готовое демо приложения с GitHub:

```bash
git clone git@github.com:bezrukavyi/spree-docker-demo.git
```

2. И переходим в директорию с приложением:

```bash
cd spree-docker-demo
```

3. Инициализируем Dockerfile и добавляем в него ранее рассмотренную структуру, где более детально описана каждая конфигурация:

```bash
touch Dockerfile
```

### Dockerfile

```Dockerfile
# Layer 0. Качаем образ Debian OS с установленным ruby версии 2.5 и менеджером для управления gem'ами bundle из DockerHub. Используем его в качестве родительского образа.
FROM ruby:2.5.1-slim

# Layer 1. Задаем пользователя, от чьего имени будут выполняться последующие команды RUN, ENTRYPOINT, CMD и т.д.
USER root

# Layer 2. Обновляем и устанавливаем нужное для Web сервера ПО
RUN apt-get update -qq && apt-get install -y \
  build-essential libpq-dev libxml2-dev libxslt1-dev nodejs imagemagick apt-transport-https curl nano

# Layer 3. Создаем переменные окружения которые буду дальше использовать в Dockerfile
ENV APP_USER app
ENV APP_USER_HOME /home/$APP_USER
ENV APP_HOME /home/www/spreedemo

# Layer 4. Поскольку по умолчанию Docker запускаем контейнер от имени root пользователя, то настоятельно рекомендуется создать отдельного пользователя c определенными UID и GID и запустить процесс от имени этого пользователя.
RUN useradd -m -d $APP_USER_HOME $APP_USER

# Layer 5. Даем root пользователем пользователю app права owner'а на необходимые директории
RUN mkdir /var/www && \
   chown -R $APP_USER:$APP_USER /var/www && \
   chown -R $APP_USER $APP_USER_HOME

# Layer 6. Создаем и указываем директорию в которую будет помещено приложение. Так же теперь команды RUN, ENTRYPOINT, CMD будут запускаться с этой директории.
WORKDIR $APP_HOME

# Layer 7. Указываем все команды, которые будут выполняться от имени app пользователя
USER $APP_USER

# Layer 8. Добавляем файлы Gemfile и Gemfile.lock из директории, где лежит Dockerfile (root директория приложения на HostOS) в root директорию WORKDIR
COPY Gemfile Gemfile.lock ./

# Layer 9. Вызываем команду по установке gem-зависимостей. Рекомендуется запускать эту команду от имени пользователя от которого будет запускаться само приложение
RUN bundle check || bundle install

# Layer 10. Копируем все содержимое директории приложения в root-директорию WORKDIR
COPY . .

# Layer 11. Указываем все команды, которые будут выполняться от имени root пользователя
USER root

# Layer 12. Даем root пользователем пользователю app права owner'а на WORKDIR
RUN chown -R $APP_USER:$APP_USER "$APP_HOME/."

# Layer 13. Указываем все команды, которые будут выполняться от имени app пользователя
USER $APP_USER

# Layer 14. Запускаем команду для компиляции статических (JS и CSS) файлов
RUN bin/rails assets:precompile

# Layer 15. Указываем команду по умолчанию для запуска будущего контейнера. По скольку в `Layer 13` мы переопределили пользователя, то puma сервер будет запущен от имени www-data пользователя.
CMD bundle exec puma -C config/puma.rb
```

### Docker-entrypoint

Команды, которые должны быть запущены перед запуском контейнера (entrypoint) мы выносим в docker-entrypoint.sh

Создадим этот файл с помощью следующей команды:

```bash
touch docker-entrypoint.sh
chmod +x docker-entrypoint.sh
```

И добавим в него команды для создания базы данных и прогона миграций Rails приложения.

docker-entrypoint.sh
```sh
#!/bin/bash
# Interpreter identifier

# Exit on fail
set -e

rm -f $APP_HOME/tmp/pids/server.pid
rm -f $APP_HOME/tmp/pids/sidekiq.pid

bundle exec rake db:create
bundle exec rake db:migrate

exec "$@"
```

### Исключить файлы, не относящиеся к сборке

Иногда нужно избежать добавления определенных файлов в образ приложения, например secretsfiles или файлов, которые относятся только к локальному окружению. Для этого есть .dockerignore. Принцип работы .dockerignore такой же как, с .gitignore.

4. Создаем файл .dockerignore

```
touch .dockerignore
```

И добавляем в него следующее

```
.git
/log/\*
/tmp/\*
!/log/.keep
!/tmp/.keep
!/tmp/pids/.keep
!/tmp/cache/.keep
/vendor/bundle
/public/assets
/config/master.key
/config/credentials.local.yml
/deploy/configs
/.bundle
/venv
```

5. Для того, чтобы упаковать наше приложение в образ, достаточно одной команды:

```
docker build -t spreeproject_server_app .
```

Где -t задает тег образу. Добавление тегов к образам помогает лучше идентифицировать образ из всех имеющихся.

Другие [полезные команды](https://docs.docker.com/develop/develop-images/dockerfile_best-practices) для работы с образами.

Итак, мы рассмотрели схему запуска нашего Spree-приложения. В следующем разделе мы научимся запускать приложение и все зависимые сервисы (PostgreSQL, Redis, API, client) с помощью одной команды.

### Запуск образа Rails-приложения и зависимых сервисов

#### Docker-compose

Работа будущего приложения зависит от работы сторонних сервисов, таких, как PostgreSQL, Redis, а также идентичное основному приложению Sidekiq-приложение. Следуя идеологии Docker, все эти сервисы должны быть изолированы от локального окружения и запущены в отдельных контейнерах, которые &quot;общаются&quot; друг с другом. Если структура проекта состоит из большого количества сервисов, то поднимать каждый отдельный Docker-сервис вручную неудобно.

Поэтому, для автоматизации процесса запуска всех сервисов будем использовать [Docker-compose](https://docs.docker.com/compose/overview/).

Docker-Compose – это инструмент для определения и запуска многоконтейнерных приложений Docker. С Compose вы используете файл YAML для настройки сервисов(контейнеров) вашего приложения. Затем с помощью одной команды вы создаете и запускаете все сервисы из своей конфигурации.


#### Полезные ссылки

Полную структура файла для Dockercompose найдете [тут](https://docs.docker.com/compose/compose-file/). Пожалуйста, ознакомьтесь с ней перед тем, как двигаться дальше по туториалу

Теперь рассмотрим структуру нашего приложения уже со стороны реализации конфигурации для Dockercompose.

Для этого создадим файл docker-compose.development.yml

```
touch docker-compose.development.yml
```

В него мы добавим рассмотренную ранее конфигурацию, где более детально комментариями описана каждая конфигурация:

```yml
# Version - версия синтаксиса compose-файла. Файл Compose всегда начинается с номера версии, который указывает используемый формат файла. Это помогает гарантировать, что приложения будет работать как ожидается, так как новые функции или критические изменения постоянно добавляются в Compose.
version: '3.1'

# Volume – дисковое пространство между HostOS и ContainerOS. Проще – это папка на вашей локальной машине примонтированная внутрь контейнера.
volumes: # Объявим volumes, которые будут доступны в сервисах
 redis:
 postgres:

# Service - запущенный контейнер
services: # Объявляем сервисы(контейнеры) которые будут запущены с помощью compose
 db:
   image: postgres:10 # В качестве образа сервиса используется официальный образ Postgresql из Docker Hub
   expose:
     - 5432 # Выделяем для postgres 5432-ый порт контейнера
   environment: # Указываем список глобальных ENV-переменных внутри текущего контейнера
     POSTGRES_USER: postgres
     POSTGRES_PASSWORD: postgres
     POSTGRES_DB: spreedemo_development
   volumes:
     - postgres:/var/lib/postgresql/data # Все данные из директории data буду ложиться в volume `postgres`
   healthcheck:
     test: ["CMD", "pg_isready", "-U", "postgres"] # Команда для проверки состояния сервиса

 in_memory_store:
   image: redis:4-alpine # В качестве образа сервиса используется официальный образ Redis из Docker Hub
   expose:
     - 6379 # Выделяем для redis 6379-ый порт контейнера
   volumes:
     - redis:/var/lib/redis/data
   healthcheck:
     test: ["CMD", "redis-cli", "-h", "localhost", "ping"]

 server_app: &server_app
   build: . # В качестве образа будет использоваться Dockerfile в текущей директории
   command: bundle exec rails server -b 0.0.0.0 # переопределяем команду запуска контейнера
   entrypoint: "./docker-entrypoint.sh" # указываем какую команду нужно запустить перед тем как контейнер запустится
   volumes:
      - .:/home/www/spreedemo # Указываем, что директория приложения в контейнере будет ссылаться на директорию приложения на Host OS (локальная нода). Таким образом, при изменение файлов из app или других директорий на вашей локальной машине, все изменения так же будут применены и на контейнер с данным сервисом.
      - /home/www/spreedemo/vendor/bundle # Исключаем монтирование установленных гемов в контейнер 
      - /home/www/spreedemo/public/assets # Исключаем монтирование сгенерированых assets в контейнер
   tty: true # Открываем доступ для деббагинга контейнера
   stdin_open: true # Открываем доступ для деббагинга контейнера
   restart: on-failure # Перезапустить контейнер в случае ошибки
   environment:
     RAILS_ENV: development
     DB_HOST: db
     DB_PORT: 5432
     DB_NAME: spreedemo_development
     DB_USERNAME: postgres
     DB_PASSWORD: postgres
     REDIS_DB: "redis://in_memory_store:6379"
     SECRET_KEY_BASE: STUB
     DEVISE_SECRET_KEY: STUB
   depends_on: # Указываем список сервисов от которых зависит текущий сервис. Текущий сервис будет запущен только после того как запустятся зависимые сервисы
     - db
     - in_memory_store
   ports:
     - 3000:3000 # Указываем что порт из контейнера будет проксироваться на порт HostOS (HostPort:ContainerPort)
   healthcheck:
     test: ["CMD", "curl", "-f", "http://localhost:3000"]

 server_worker_app:
   <<: *server_app # Наследуемся от сервиса server_app
   command: bundle exec sidekiq -C config/sidekiq.yml
   entrypoint: ''
   ports: []
   depends_on:
     - db
     - server_app
     - in_memory_store
   healthcheck:
     test: ["CMD-SHELL", "ps ax | grep -v grep | grep sidekiq || exit 1"]
```

#### Контейнеры и volumes

Несмотря на то, что все приложение упаковано в образ и запущено в изолированном контейнере, нам по-прежнему доступен railshotreloader. Все потому, что мы воспользовались Volumes. Мы указали, что директория app и директория vendor/assets из запущенного контейнера будут ссылаться на локальную директорию HostOS.

Теперь можно запустить всю инфраструктуру приложения, выполнив команду:

```
docker-compose -f docker-compose.development.yml -p spreeproject up
```

`-p` указывает, какой префикс добавить контейнерам. Желательно использовать подобный контекст, чтобы когда проектов на Docker станет больше, вам было проще ориентироваться по контексту;
`-f` указывает, какой docker-compose файл использовать.

Здесь вы найдете [другие полезные команды](https://docs.docker.com/compose/reference) для взаимодействия с compose.

Проверить состояние запущенных сервисов мы можем с помощью следующей команды:

```bash
docker-compose -f docker-compose.development.yml -p spreeproject ps
```

Когда все сервисы буду в статусе healthy

![compose_ps](images/new/compose_ps.jpg)

Приложение будет доступно по адресу `localhost:3000`

## Подведем итог

Итак, в первой части туториала мы рассмотрели:

- Принцип работы Docker и его компоненты. Преимущества и недостатки работы с Docker в сравнении с виртуальными машинами;
- Пошаговую сборку Rails-приложения в Dockerfile;
- Запуск образа Rails-приложения и зависимых сервисов с помощью Docker compose.

В следующих частях мы продолжим развертывание приложения с помощью сервисов AWS. Не пропустите вторую часть туториала!
