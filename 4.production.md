# 4 Production

В предыдущей главе туториала мы:
- Реализовали возможность хранения sensitive data приложения
- Создали Docker образ для веб сервера Nginx
- Подготовили конфигурацию для развертывания staging инфраструктуры на AWS.
- Запустили staging приложение на AWS.

## Какую задачу решаем?
Мы развернули staging приложение. Протестировали его, все работает. Теперь необходиом развернуть готовое к масштабированию production приложения. Так же, поскольку поддерживаемых енвайремнтов стало больше, нужно упростить процесс обновелния приложения, путем автоматизации деплоя.

## Постановка задач
Стек сервисов production-приложения остается таким же как и на staging. Инфраструктура имеет следующие отличия:
- *Cloud database*. В качестве базы данных для нашего приложения мы будем использовать [Amazon RDS](https://aws.amazon.com/rds). Ккоторый упрощает настройку, использование и масштабирование реляционной базы данных в облаке.
- *Cloud in-memory storage*. В качестве in-memory store мы будем использовать [Amazon ElastiCache](https://aws.amazon.com/elasticache) который облегчает развертывание и запуск в облаке серверных узлов, путем предоставления полностью управляемых сред Redis и Memcached.
- *Масштабирование и балансировщик нагрузки*. В случае увеличения количества запросов, мощностей одного сервера может быть недостаточно. Поэтому в случае увеличения количества серверов нужен сервис который будет распределять запросы между несколькими нодами в зависимости от их нагрузки. Будем использовать [Amazon Elasticloadbalancing](https://aws.amazon.com/elasticloadbalancing)
- *Декомпозиция*. Если на предыдущих этапах все зависимые сервисы приложения размещались на одном инстансе, то в условиях горизонтальной масштабируемости приложения нужно увеличивать количества сервисов только тех на которые прихитсяя наибольшая нагрузка. В контексте нашей инфраструктуры необходимо вынести в отдельные инстансы Server App и Worker App, потому что не всегда когда нужно масштабировать Server App, нужно масштабировать и Worker App, и наоборот.

Архитектура production приложения преображается в следующую структуру.

![aws_structure](images/aws_structure.jpg)

Таким образом для корректной и безопасной работы production окружения нам понадобится:
Для этого нам понадобится:
- Сохранить все переменные production окружения в зашифрованном виде
- Создать Postgresql инстанс с помощью RDS
- Создать Redis инстанс с помощью ElasticCache
- Создать кластер для production приложения на AWS ECS
- Создать compose файл для запуска приложения и его зависимых сервисов на AWS ECS
- Запустить сервис с production версией приложения на AWS
- Интегрировать [CircleCI](https://circleci.com/) для автоматизации деплоя

Стек технологий который мы будем использовать:
- Docker
- Docker Compose
- Ruby
- Rails
- Nginx
- Sidekiq
- AWS CLI
- ECS CLI
- ElasticCache
- RDS
- ELB
- ECS
- EC2
- ECR
- S3

Начнем с добаления AWS credentials в файл с глобальными переменными приложения используя команду `EDITOR=nano rails credentials:edit`.

```yml
production:
  # ...
  AWS_ACCESS_KEY_ID: 'YOUR_AWS_ACCESS_KEY_ID'
  AWS_SECRET_ACCESS_KEY: 'YOUR_AWS_SECRET_ACCESS_KEY'
  DEVISE_SECRET_KEY: 'YOUR_DEVISE_SECRET_KEY'
  SECRET_KEY_BASE: 'YOUR_SECRET_KEY_BASE'
```

## Security Groups

```bash
# GroupId группы load balancer обозначим, как `$BALANCER_SG`
aws ec2 create-security-group \
  --group-name spreeproject-production-balancer \
  --description "Spree project Production Balancer"

# GroupId группы серверного приложения обозначим, как `$SERVER_APP_SG`
aws ec2 create-security-group \
  --group-name spreeproject-production-server-app \
  --description "Spree project Production Server App"

# GroupId группы баз данных приложения обозначим, как `$DB_SG`
aws ec2 create-security-group \
  --group-name spreeproject-production-db \
  --description "Spree project Production DB"

# GroupId группы для memory stores сервиса обозначим, как `$STORE_SG`
aws ec2 create-security-group \
  --group-name spreeproject-production-store \
  --description "Spree project Production Memory Store"
```

Коммуникация клиента с инфраструктурой нашего приложения будет происходить через [elasticloadbalancing](https://aws.amazon.com/elasticloadbalancing). В целях безопасности, доступ ко всем остальным сервисам будет доступен только в рамках внутренней сети AWS и определенных security groups. Исходя из схемы, сконфигурируем правила общения между сервисами:

Исходя из диагрммы сконфигурируем правила общения между сервисами

```bash
# Откроем доступ любому клиенту на 80-й порт балансера.
aws ec2 authorize-security-group-ingress \
  --group-id $BALANCER_SG \
  --protocol tcp \
  --port 80 \
  --cidr 0.0.0.0/0

# Откроем доступ балансеру на 8080-й порт будущим инстансам, где будет запущен Nginx.
aws ec2 authorize-security-group-ingress \
  --group-id $SERVER_APP_SG \
  --protocol tcp \
  --port 8080 \
  --source-group $BALANCER_SG

# Откроем доступ будущим инстансам с приложением к RDS, где по 5432 порту будет доступен Postgres.
aws ec2 authorize-security-group-ingress \
  --group-id $DB_SG \
  --protocol tcp \
  --port 5432 \
  --source-group $SERVER_APP_SG

# Откроем доступ будущим инстансам с приложением к ElastiCache, где по 6379 порту будет доступен Redis.
aws ec2 authorize-security-group-ingress \
  --group-id $STORE_SG \
  --protocol tcp \
  --port 6379 \
  --source-group $SERVER_APP_SG
```

## S3

Для того хранения изображений наше приложение будет использовать S3-bucket. Создадим его с помощью следующей команды

```bash
aws s3api create-bucket --bucket spreeproject-production
```

После создания bucket на S3, обновим переменные credentials:

```yml
production:
  # ...
  S3_BUCKET_NAME: 'spreeproject-production'
  S3_REGION: 'us-east-1'
  S3_HOST_NAME: 's3.amazonaws.com'
```

## RDS

Amazon предоставляет управляемый сервис [RDS](https://aws.amazon.com/rds), который упрощает настройку, использование и масштабирование реляционной базы данных.

Чтобы начать работать вам достаточно просто создать RDS инстанс с выбранной вами базой данных и получить хост сервиса для коннекшина вашего приложения к нему. 

В качестве базы данных будем использовать RDS инстанс с готовой сборкой для Postgres. В $YOUR_DB_USERNAME и $YOUR_DB_PASSWORD укажите собственные значения.

```bash
aws rds create-db-instance \
  --engine postgres \
  --no-multi-az \
  --vpc-security-group-ids $DB_SG \
  --db-instance-class db.t2.micro \
  --allocated-storage 20 \
  --db-instance-identifier spreeproject-production \
  --db-name spreeproject_production \
  --master-username $YOUR_DB_USERNAME \
  --master-user-password $YOUR_DB_PASSWORD
```

Выполним команду для проверки статуса RDS инстанса:

```bash
aws rds describe-db-instances
```

В отличие от EC2-инстансов, RDS будет доступен через некоторое время и после успешной инициализации будет доступен его `endpoint`:

Обновим наш файл с encrypted credentials: 

```yml
production:
  # ...
  DB_NAME: 'spreeproject_production'
  DB_USERNAME: 'YOUR_DB_USERNAME'
  DB_PASSWORD: 'YOUR_DB_PASSWORD'
  DB_HOST: 'YOUR_RDS_ENDPOINT'
  DB_PORT: '5432'
```

## ElastiCache

Amazon предоставляет управляемый сервис [ElastiCache](https://aws.amazon.com/ru/elasticache/) который облегчает развертывание и запуск в облаке Memcached или Redis. 

В качестве in-memory store будем использовать сервис с готовой сборкой Redis:

```bash
aws elasticache create-cache-cluster \
  --engine redis \
  --security-group-ids $STORE_SG \
  --cache-node-type cache.t2.micro \
  --num-cache-nodes 1 \
  --cache-cluster-id spree-production
```

Команда для проверки статуса инстанса:

```bash
aws elasticache describe-cache-clusters --show-cache-node-info
```

На инициализацию ElastiCache также потребуется некоторое время. После успешной инициализации ElastiCache сохраним полученный `Address` сервиса в encrypted credentials:

```yml
production:
  # ...
  REDIS_DB: "redis://YOUR_EC_ENDPOINT:6379"
```

## ELB

В случае увеличения количества запросов, мощностей одного сервера может быть недостаточно. Это приводит к увеличению времени обслуживания запросов. Чтобы устранить эту проблему, подключим к нашей инфраструктуре балансировщик нагрузки (load balancer), который будет распределять запросы между несколькими нодами в зависимости от их нагрузки. У AWS есть готовое решение [Elasticloadbalancing](https://aws.amazon.com/elasticloadbalancing).

![load_balancer](images/load_balancer.jpg)

Перед его созданием нужно получить список instances subnets, и выбрать те у которых `DefaultForAz` параметр имеет значение `true`.

```bash
aws ec2 describe-subnets
```

```bash
# Создаем непосредственно сам loadbalancer
aws elb create-load-balancer \
  --load-balancer-name spreeproject-balancer-production \
  --listeners "Protocol=HTTP,LoadBalancerPort=80,InstanceProtocol=HTTP,InstancePort=8080" \
  --subnets $YOUR_SUBNETS \
  --security-groups $BALANCER_SG

# Добавим к нему connection settings:
aws elb modify-load-balancer-attributes \
  --load-balancer-name spreeproject-balancer-production \
  --load-balancer-attributes "{\"ConnectionSettings\":{\"IdleTimeout\":5}}"
```

Состояние созданного балансировщика можно проверить с помощью команды:

```bash
aws elb describe-load-balancers
```

В дальнейшем, наше Spree приложение будет доступно через его `DNSName`.

## ECS Cluster

Работа с ECS Cluster и создание в нем сервисов идентичная главе где разворачивали staging приложение

Создадим конфигурацию для будущего кластера `spreeproject-production` введя следующую команду в консоле:

```bash
CLUSTER_NAME=spreeproject-production

ecs-cli configure --region us-east-1 --cluster $CLUSTER_NAME --config-name $CLUSTER_NAME
```

После создадим кластер `spreeproject-production` к которому будет подвязан один EC2 инастанс типа `t2.micro`. Для этого вводим в терминале следующую команду:

```bash
ecs-cli up \
  --keypair spreeproject_keypair \
  --capability-iam \
  --size 3 \
  --instance-type t2.micro \
  --vpc $AWS_VPC \
  --subnets $AWS_SUBNETS \
  --image-id ami-0a6be20ed8ce1f055 \
  --security-group $SERVER_APP_SG \
  --cluster-config $CLUSTER_NAME \
  --verbose
```

## ECR

Создадим репозитории для хранения образов production версии приложения:

```bash
# Предварительно нужно аутентифицироваться с помощью следующей команды:
$(aws ecr get-login --region us-east-1 --no-include-email)
```

Актуализируем образы наших сервисов, вызвав команду:

```bash
docker-compose -p spreeproject -f docker-compose.development.yml build
```

После загрузим локальный образ в репозиторий `YOUR_ECR_ID.dkr.ecr.us-east-1.amazonaws.com`. `YOUR_ECR_ID` - `registryId` созданного репозитория

```bash
docker tag spreeproject_server_app:latest $YOUR_ECR_ID.dkr.ecr.us-east-1.amazonaws.com/spreeproject/server_app:production
docker push $YOUR_ECR_ID.dkr.ecr.us-east-1.amazonaws.com/spreeproject/server_app:production
```

Сделаем тоже самое для `web_server`, в котором будет образ Nginx

```bash
aws ecr create-repository --repository-name spreeproject/web_server
```

```bash
docker tag spreeproject_web_server:latest $YOUR_ECR_ID.dkr.ecr.us-east-1.amazonaws.com/spreeproject/web_server:production
docker push $YOUR_ECR_ID.dkr.ecr.us-east-1.amazonaws.com/spreeproject/web_server:production
```

## ECS Tasks

После создадим `docker-compose.production.yml` как compose production версии приложения. 
**Замените** `YOUR_ECR_ID` и `YOUR_RAILS_MASTER_KEY` на собственные значения

```bash
cd ../docker && touch docker-compose.production.yml
```

```yml
version: '3'

services:
  web_server:
    image: YOUR_ECR_ID.dkr.ecr.us-east-1.amazonaws.com/spreeproject/web_server:production
    ports:
      - 8080:8080
    links:
      - server_app

  server_app:
    image: YOUR_ECR_ID.dkr.ecr.us-east-1.amazonaws.com/spreeproject/server_app:production
    command: bundle exec puma -C config/puma.rb
    entrypoint: "./docker-entrypoint.sh"
    expose:
      - 3000
    environment:
      RAILS_ENV: production
      RAILS_MASTER_KEY: YOUR_RAILS_MASTER_KEY
```

Создадим файл схему ecs-params

```bash
touch ecs-params.production.yml
```

```yml
version: 1
task_definition:
  ecs_network_mode: bridge

  task_size:
    cpu_limit: 768
    mem_limit: 0.5GB

  services:
    web_server:
      essential: true
    server_app:
      essential: true
```

## ECS Services

Создаем задачу для будущего сервиса ECS

```bash
# create task definition for a docker container
ecs-cli compose \
  --file docker-compose.production.yml \
  --project-name $CLUSTER_NAME \
  --ecs-params ecs-params.production.yml \
  --cluster-config $CLUSTER_NAME \
  create
```

После создания задачи, ей будет присвоенный определнный номер. Запишим этот номер в переменную TASK_NUMBER

И создадим сервис ECS

```bash
aws ecs create-service \
  --service-name 'spreeproject' \
  --cluster $CLUSTER_NAME \
  --task-definition "$CLUSTER_NAME:$TASK_NUMBER" \
  --load-balancers "loadBalancerName=$BALANCER_NAME,containerName=web_server,containerPort=8080" \
  --desired-count 2 \
  --deployment-configuration "maximumPercent=200,minimumHealthyPercent=50"
```

Теперь на оставшемся незанятом инстансе нашего кластера, запустим приложения для background processing

Создадим `docker-compose-worker.production.yml` как compose production версии Sidekiq приложения. **Замените** `YOUR_ECR_ID` и `YOUR_RAILS_MASTER_KEY` на собственные значения

```bash
touch docker-compose-worker.production.yml
```

```yml
version: '3'

services:
  worker_app:
    image: YOUR_ECR_ID.dkr.ecr.us-east-1.amazonaws.com/spreeproject/server_app:production
    command: bundle exec sidekiq -C config/sidekiq.yml
    environment:
      RAILS_ENV: production
      RAILS_MASTER_KEY: YOUR_RAILS_MASTER_KEY
```

Создадим файл схему ecs-params

```bash
touch ecs-params-worker.production.yml
```

```yml
version: 1
task_definition:
  ecs_network_mode: bridge

  task_size:
    cpu_limit: 768
    mem_limit: 0.5GB

  services:
    worker_app:
      essential: true
```

Создаем задачу для будущего сервиса ECS

```bash
# create task definition for a docker container
ecs-cli compose \
  --file docker-compose-worker.production.yml \
  --project-name "$CLUSTER_NAME-worker" \
  --ecs-params ecs-params-worker.production.yml \
  --cluster-config $CLUSTER_NAME \
  create
```

И создадим сервис для этой задачи

```bash
aws ecs create-service \
  --service-name "spreeproject-worker" \
  --cluster $CLUSTER_NAME \
  --task-definition "$CLUSTER_NAME-worker:$TASK_NUMBER" \
  --desired-count 1 \
  --deployment-configuration "maximumPercent=200,minimumHealthyPercent=50"
```
